package tacos.data;

import tacos.Order;

import java.util.List;

public interface OrderRepository {

    List<Order> findAll();

    Order save(Order order);

}
