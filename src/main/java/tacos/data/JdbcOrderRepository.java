package tacos.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import tacos.Order;
import tacos.Taco;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: довести функционал получения заказов до конца
@Repository
public class JdbcOrderRepository implements OrderRepository {

    private JdbcTemplate jdbc;
    private SimpleJdbcInsert orderInserter;
    private SimpleJdbcInsert orderTacoInserter;
    private ObjectMapper objectMapper;

    @Autowired
    public JdbcOrderRepository(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
        this.orderInserter = new SimpleJdbcInsert(jdbc)
                .withTableName("Taco_Order")
                .usingGeneratedKeyColumns("id");
        this.orderTacoInserter = new SimpleJdbcInsert(jdbc)
                .withTableName("Taco_Order_Tacos");
        this.objectMapper = new ObjectMapper();
    }

    @Override
    public List<Order> findAll() {
        return jdbc.query("select id, deliveryName from Taco_Order", this::mapRowToTacoOrder);
    }

    @Override
    public Order save(Order order) {
        order.setPlacedAt(new Date());
        long orderId = saveOrderDetails(order);
        order.setId(orderId);
        List<Taco> tacos = order.getTacos();
        for (Taco taco : tacos) {
            saveTacoToOrder(taco, orderId);
        }
        return order;
    }

    /**
     * executeAndReturnKey and execute
     * принимают Map<String, Object> where key corresponds to a column names in the table
     * map values are inserted into those columns
     */
    private long saveOrderDetails(Order order) {
        @SuppressWarnings("uncheked")
        Map<String, Object> values = objectMapper.convertValue(order, Map.class);
        // This is necessary because ObjectMapper would otherwise convert the Date property into a long,
        // which is incompatible with the placedAt field in the Taco_Order table.
        values.put("placedAt", order.getPlacedAt());

        return orderInserter
                .executeAndReturnKey(values)
                .longValue();
    }

    private void saveTacoToOrder(Taco taco, long orderId) {
        Map<String, Object> values = new HashMap<>();
        values.put("tacoOrder", orderId);
        values.put("taco", taco.getId());
        orderTacoInserter.execute(values);
    }

    private Order mapRowToTacoOrder(ResultSet rs, int rowNum) throws SQLException {
        Order order = new Order();
        order.setId(rs.getLong("id"));
        order.setName(rs.getString("deliveryName"));
        return order;
    }

}
